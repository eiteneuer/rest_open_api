import pyautogui as p
import time
import sys
#p.size()


p.PAUSE = 0.3
p.FAILSAFE = True

select_all = ('ctrl', 'a')
copy = ('ctrl', 'c')
paste = ('ctrl', 'v')
publish = ('ctrl', 'enter')

if __name__ == '__main__':
    bitbucket_button = (220, 25)
    
    swaggedit_button = (320, 25)
    inside_swag = (swaggedit_button[0], 225)
    
    conf_button = (420, 25)
    conf_edit = (1690, 150)

    p.moveTo(*bitbucket_button)
    p.click()
    p.hotkey('f5')
    time.sleep(0.3)
    p.hotkey(*select_all)
    p.hotkey(*copy)
    
    if 'swagger' in sys.argv:
        p.moveTo(*swaggedit_button)
        p.click()
        p.moveTo(*inside_swag)
        p.click()
        p.hotkey(*select_all)
        p.hotkey('backspace')
        p.hotkey(*paste)
    elif 'conf' in sys.argv:
        #p.moveTo(*conf_button)
        p.click(*conf_button)
        p.click(*conf_edit)
        time.sleep(3)
        p.click(400, 400, 2) #double click
        p.hotkey('backspace')
        p.click(clicks=2)
        p.hotkey(*paste)
        time.sleep(0.5)
        p.click(clicks=2)
        p.hotkey(*paste)
        p.hotkey(*publish)


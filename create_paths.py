import json
import sys

testdaten = {
    'url': '/machine/{machine_id}/execute/{execution_channel}',
    'html_cmd' : 'post',
    'tags': 'execution',
    'summary': 'summary',
    'description': '',
    'params': [
        {
            'name' : 'bing',
            'troll' : 'lollol'
        },
        {
            'name' : 'pen',
            'ein' : 'bisschen'
        }
    ]
}

level = 8
indent0 = (level - 2) * ' '
indent1 = level * ' '
indent2 = (level + 2) * ' '
indent3 = (level + 4) * ' '

def whitespaces(n):
    return n * ' '

def create_param(data):
    #not used, deprecated
    s = indent1 + '- name: ' + data['name'] +'\n'
    for i, (key, value) in enumerate(data.items()):
        if key == 'name':
            continue
        kv_pair = key + ': ' + value + '\n'
        s += indent2 + kv_pair
    return s

def create_path(data, include_url=True):
    if include_url:
        s = '\n  {url}:'.format(**data)
    else:
        s = ''
    s += '''
    {html_cmd}:
      tags:
      - "{tags}"
      summary: "{summary}"
      description: "{description}"
      operationId: "{operation_id}"
      consumes:
        - "application/json"
      produces:
        - "{return_type}"\n'''.format(**data)

    if len(data['params']) > 0:
        s += indent0 + 'parameters:\n'
        for par in data['params']:
            #unfortunately, format is a reserved keyword in swagger, so we
            #cant name a reference "format". We rename this to "ref_format"
    
            if par['name'] == 'format':
                s += indent1 + '- $ref: "#/parameters/ref_%s"\n' % par['name']
            else:
                s += indent1 + '- $ref: "#/parameters/%s"\n' % par['name']
           
    #s += indent1 + 'schema:\n'
    #s += indent1 + '  $ref: definitions/x'

    s += indent0 + 'responses:\n'
    s += indent1 + '401:\n'
    s += indent2 + 'description: "not authorized Request! Your Session probably expired and you need to login again or request GET `/ping` while Session is still active"\n'
    s += indent1 + '404:\n'
    s += indent2 + 'description: "Bad request, Invalid input"\n'
    s += indent1 + '500:\n'
    s += indent2 + 'description: "Internal Server Error"\n'

    return s

if __name__ == '__main__':
    verbose = True
    if '-quiet' in sys.argv:
        verbose = False
    
    dont_include = ['demo']

    #main data file containing routes information
    with open('./jsons/routes_flattened.json', 'r') as fp:
        data = list(json.load(fp).values())

        for dont_incl in dont_include:
            print('INFO: excluding all commands that include:', dont_incl)
            data = [d for d in data if dont_incl not in d['url']]
        #sort list by url. data is a list, one element for each route
        data = sorted( data, key=lambda url: url['url'])
        print('INFO: parsed %s routes from routes.txt' % len(data))

    #create header for rest-api.yaml
    with open('./yamls/swaggerversion_info_host_basepath.yaml','r') as fp:
        s = ''.join(fp.readlines())

    #collect information about tags
    with open('./yamls/tags.yaml','r') as fp:
        s += ''.join(fp.readlines())

   #create parameter definitions
    with open('./yamls/parameters.yaml', 'r') as fp:
        s += ''.join(fp.readlines())
        
    with open('./jsons/routes_descriptions.json', 'r') as fp:
        #get description+summary for each route, append them to data
        routes_info = json.load(fp)
        for i in range(len(data)):
            op_id = data[i]['operation_id']
            data[i]['summary'] = routes_info[op_id]['summary']
            data[i]['description'] = routes_info[op_id]['description']
            data[i]['tags'] = routes_info[op_id]['tags']
            data[i]['return_type'] = routes_info[op_id]['return_type']
            if verbose: print(data[i]['url'], data[i]['summary'])


    s  += 'paths:'
    last_url = ''

    for i, dat in enumerate(data):
        include_url = (data[i-1]['url'] != dat['url'])
        if verbose: print(include_url)
        s += create_path(dat, include_url)    

    output = open('rest-api.yaml', 'w')
    output.write(s)
    #print(create_path(testdaten))
    if verbose: print(s)



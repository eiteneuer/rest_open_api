import read_in_routes_txt 
import sys

def create_parameter_yaml(replace_dict):
    ids = list(set(replace_dict.values()))
    s = 'parameters:\n'
    for par in ids:
        s += '  ' + par + ':\n'
        s+=  '    name: ' + par + '\n'
        s += '    in: paths\n'
        s += '    required: true\n'
        s += '    type: string\n'
        s += '    description: %s\n' % par.split('_')[0]
        #s += '    minimum: 0\n'
        #s += '    maximum: 100\n'
        #s += '    default: 100\n'
    if '-f' in sys.argv:
        mode = 'w'
    else:
        mode = 'x'
    with open('./yamls/parameters.yaml', mode) as file:
        file.write(s)
    return s 


 
if __name__ == '__main__':
    replace = read_in_routes_txt.replace_dict
    default = read_in_routes_txt.default_dict
    replace_dict = {**replace, **default}
    res = create_parameter_yaml(replace_dict)


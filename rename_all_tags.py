RENAME = {
    "1.01 Gateway" : "1.01 Gateway-Access",
    "1.00 Authentication" : "1.02 Gateway-Authentication",
    "4.00 Gateway Administration": "1.05 Gateway Administration",
    "1.07 User": "1.06 User",
    "1.11 GraphComponent types": "2.01 Configuration-Types",
    "1.04 Configuration": "2.02 Configuration-Configurations",
    "1.04 Configuration": "2.02 Configuration-Configurations",
    "1.04 Advanced Configuration": "2.02 Configuration-Configurations",
    "1.06 Machine": "2.03 Configuration-Machine",
    "1.02 Command": "2.04 Configuration-Command",
    "1.03 Component": "2.05 Configuration-Component",
    "1.10 Topic": "2.06 Configuration-Topic",
    "2.00 Config Control": "3.01 Control-ConfigurationControl",
    "2.01 Machine Control": "3.02 Control-MachineControl",
    #4.01 manual, 4.03 manual
    "2.02 Execution": "4.02 Script-ExecutionState",
    #5.02 manual
    "Session Data" : "5.01 Data-Session",
    "3.00 Machine Tests" : "6.01 Tests-SLM Machine Tests",
    "3.01 Demo" : "6.02 Tests-Demo Tests",
    "1.09 Project" : "7.01 Projects",
    "1.05 Job": "7.02 Jobs",
    "1.08 Parts": "7.03 Parts",

}

from subprocess import call

if __name__ == '__main__':
    #command = 'sed -i 's/"Configuration"/"1.04 Configuration"/g' yamls/tags.yaml jsons/routes_descriptions.json'
    #cmd = ['sed', '-i', '\'s/bing/omg/g\'', 'eins',
    #       'zwei']
    #call(cmd)
   
    for key, value in RENAME.items():
        #cmd = "sed -i \'s/%s/%s/g\' yamls/tags.yaml jsons/routes_descriptions.jsona" % (1, 2)
        cmd = ['sed', '-i', 's/%s/%s/g' % (key,value), 'yamls/tags.yaml',
               'jsons/routes_descriptions.json']
        print('executing:', cmd)
        call(cmd)

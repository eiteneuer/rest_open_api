import json

with open('routes.json', 'r') as fp:
    data = json.load(fp)

testdaten = {
    'url': '/machine/{machine_id}/execute/{execution_channel}',
    'html_cmd' : 'post',
    'tags': 'execution',
    'summary': 'summary',
    'description': '',
    'params': [
        {
            'name' : 'bing',
            'troll' : 'lollol'
        },
        {
            'name' : 'pen',
            'ein' : 'bisschen'
        }
    ]
}

level = 8
indent0 = (level - 2) * ' '
indent1 = level * ' '
indent2 = (level + 2) * ' '
indent3 = (level + 4) * ' '

def whitespaces(n):
    return n * ' '

def create_param(data):
    s = (level - 2) *' ' + '- name: ' + data['name'] +'\n'
    for i, (key, value) in enumerate(data.items()):
        if key == 'name':
            continue
        kv_pair = key + ': ' + value + '\n'
        s += indent1 + kv_pair
    return s

def create_path(data, include_url=True):
    if include_url:
        s = '\n  {url}:'.format(**data)
    else:
        s = ''
    s += '''
    {html_cmd}:
      tags:
      - "{tags}"
      summary: "{summary}"
      description: "{description}"
      operationId: "{tags}"
      consumes:
        - "application/json"
      produces:
        - "application/json"\n'''.format(**data)

    if len(data['params']) > 0:
        s += indent0 + 'paramaters:\n'
        for par in data['params']:
            s += create_param(par)

    #s += indent1 + 'schema:\n'
    #s += indent1 + '  $ref: definitions/x'

    s += indent0 + 'responses:\n'
    s += indent1 + '405:\n'
    s += indent2 + 'description: "Invalid input"'
    return s

with open('routes_flattened.json', 'r') as fp:
    data = list(json.load(fp).values())

    #sort list by url
    data = sorted( data, key=lambda url: url['url'])

s = whitespaces(2) + 'paths:'
last_url = ''

for i, dat in enumerate(data):
    include_url = (data[i-1]['url'] != dat['url'])
    print(include_url)
    s += create_path(dat, include_url)    

output = open('rest-api.yaml', 'w')
output.write(s)
#print(create_path(testdaten))
print(s)


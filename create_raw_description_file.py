import json
import sys
import time
import shutil

output = './jsons/routes_descriptions.json'

with open('./jsons/routes_flattened.json', 'r') as fp:
    data = list(json.load(fp).values())
    data = sorted( data, key=lambda url: url['url'])

if '-add' in sys.argv:
    with open(output, 'r') as fp:
        description = json.load(fp)
    with open(output, 'w') as fp:
        for op_id in description:
            description[op_id]['return_type'] = 'json'
            pass
        json.dump(description, fp, indent = 3)
else:
    description = {}
    for route in data:
        #description
        name = route['operation_id'] 
        short_name = name.split('.')[-1]
        description[name] = {}
        description[name]['url'] = route['html_cmd'] + ' ' + route['url']
        description[name]['tags'] = short_name
        description[name]['summary'] = short_name + ' summary'
        description[name]['description'] = short_name + ' description'

    mode = 'x'
    if '-f' in sys.argv:
        mode = 'w'
        shutil.copyfile(output, output + '.backup.' + str(time.time()))
    with open(output, mode) as file:
        json.dump(description, file, indent=3)

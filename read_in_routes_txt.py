import json
import copy
import sys

def printer(data, indent = 3):
    print(json.dumps(data, indent = indent))

keywds = {'h1': 15, 
          'h2': '#############',
          'comment': '#',
         }

replace_url = {'mid' : 'machine_id',
               'cid' : 'config_id',
               'confid': 'component_id',
               'tid': 'topic_id',
               'eid': 'execution_channel',
               'jid': 'job_id',
               'subpIdx': 'subpart_index',
               'pid': 'part_id',
               'wid': 'workunit_id',
              }

default_dict = {
           'mid' : 'machine_id',
           'cid' : 'config_id',
           'confid': 'component_id',
           'tid': 'topic_id',
           'eid': 'execution_channel',
           'jid': 'job_id',
           'subpIdx': 'subpart_id',
           'pid': 'part_id',
           'wid': 'workunit_id',
           'id': 'id',
           
          }

replace_dict = {
    'channel': 'execution_channel',
    'machines': 'machine_id',
    'configurations' : 'config_id',
    'components' : 'component_id',
    'sessions' : 'session_id',
    'configId' : 'config_id',
    'users' : 'user_id',
    'parts' : 'part_id',
    'focus' : 'config_id',
    'size' : 'config_id',
    'circle' : 'config_id',
    'laser' : 'config_id',
    'delay' : 'config_id',
    'demoexecute' : 'demo_channel_id',
    'demousers' : 'demo_user_id',
    'jobIdx' : 'job_id',
    'hub' : 'hub_id',
    'sensorId' : 'sensor_id',
    'subpIdx' : 'subpart_id',
    'z' : 'height',
    'quality': 'quality',
    'jobs' : 'job_id',
    'projects' : 'project_id',
    'cmd' : 'command_id',
    'execute' : 'execution_channel',
    'stop' : 'execution_channel',
    'resume' : 'execution_channel',
    'pause' : 'execution_channel',
    'script' : 'execution_channel',
}

def get_params(paramstr):
    print('\n\n\nPARS:', paramstr)
    copy = paramstr
    if 'eid' in copy:
        #print(200*'X' +'\n', paramstr, '\n', params, '\n')
        #sys.exit()
        pass
    paramstr = [s for s in paramstr.split('/') if s != '']
    params = []
    for i, entry in enumerate(paramstr):
        if ':' not in entry:
            continue
        if paramstr[i-1] in replace_dict:
            par = replace_dict[paramstr[i-1]]
        else:#use default
            par = default_dict[entry.replace(':','')]
        params.append(par)
    print('FOUND PARS:', params, '\n')
    return params

turl = '/sessions/:sid/configId/:cid/jobIdx/:jidx/hub/:hub/sensorId/:sensorId/subpIdx/:subpIdx/z/:height/quality/:quality'
def fix_url(url, verbose=False):
    if verbose: print('URL:',url)
    urllist = [s for s in url.split('/') if s != '']
    new = []
    for i in range(len(urllist)):
        entry = urllist[i]
        if ':' not in entry:
            new.append(entry)
            continue
        prev = urllist[i-1]
        if prev in replace_dict:
            par = replace_dict[prev]
        elif entry[1:] in default_dict:#use default
            par = default_dict[entry[1:]]
        else:
            par = entry.replace(':','')
        new.append(':' + par)
        if verbose: print(new)
    url = '/' + '/'.join(new)
    return url



def fix_urls(L):
    for i in range(len(L)):
        if '/' in L[i]:#found url
            line = L[i].split(' ')
            line[1] = fix_url(line[1])
            L[i] = ' '.join(line)
    return L

if __name__ == '__main__':
    verbose = True
    if '-quiet' in sys.argv: verbose=False

    u2 = fix_url(turl)
    lines = open('routes2.txt').readlines()
    #get rid of \n
    lines = [l.strip() for l in lines]

    #get rid of commented lines
    lines = [l for l in lines if l.count('#') != 1]
    lines = [l for l in lines if 'TODO' not in l] 

    #get rid of emtpy lines
    lines = [l for l in lines if l != '']

    #get rid of additional whitespaces
    lines = [' '.join(l.split()) for l in lines]

    #remaps mid -> machine_id    and so forth
    orig = lines[:]
    lines = fix_urls(lines)
    '''
    if verbose:
        for i in range(len(orig)):
            print('\n')
            print(orig[i])
            print(lines[i])
    '''
    standard_command = {'tags': 'tags',
                        'summary': 'summary',
                        'description': 'description',
                        'params' : [],
                       
                       }

    standard_param = {'name': 'name',
                      'in': 'path',
                      'description': '"desc"',
                      'required': 'true',
                      'type': '"string"',
                     }
    data = {}


    foundh1 = False
    foundh2 = False
    i = 0; count = 0
    while i < len(lines):
        line = lines[i]

        #get rid of additional whitespaces
        content = line.split(' ')
        if lines[i].count('#') > keywds['h1'] and lines[i+2].count('#') > keywds['h1']:
            h1 = ' '.join(lines[i+1].split(' ')[1:-1])
            if verbose: print(lines[i+1])
            data[h1] = {} #copy.deepcopy(default)
            i += 3
            continue

        if keywds['h2'] in content and len(content) == 2:

            #if we have empty h2 (see CONFIGURATION example)
            if lines[i+1].count('#') > keywds['h1'] and lines[i+3].count('#') > keywds['h1']:
                i += 1
                continue

            h2 = content[1]
            if verbose: print(line)
            data[h1][h2]= []
            i += 1
            continue
        
        #HTML COMMAND
        if lines[i].count('#') > keywds['h1'] and lines[i+2].count('#') > keywds['h1']:
            print('should not happen often')
            print(lines[i-2:i+2])
            continue

        count += 1
        try:
            data[h1][h2].append(copy.deepcopy(standard_command))
        except KeyError: #no h2 for this h1
            h2 = h1 #just use the top header ... 
            data[h1][h2] = []
            data[h1][h2].append(copy.deepcopy(standard_command))

        #this is a dict for all info of one complete command.. e.g 'GET /login studio.blba'
        current_dict = data[h1][h2][-1]

        current_dict['html_cmd'] = content[0].lower()#need lowercase for confluence
        current_dict['url'] = content[1]
        current_dict['tags'] = h1

        #now we parse the params
        paramstr = content[1]
        if 'pp' in line:
            current_dict['operation_id'] = ' '.join(content[2:]).replace('"','\'')
            params = []
            #params = ['path', 'file']
        else:
            try:
                current_dict['operation_id'] = content[2].split('(')[0]
                params = [x[1:] for x in paramstr.split('/') if ':' in x]
            except IndexError:
                print('ERROR', line,'<line end>', content)


        if verbose: print('ORIGINAL:', paramstr)
        if verbose: print('FOUND:', params)
        if len(params) == 0:
            pass 
        elif len(params) > 0:
            for par in params:
                current_dict['params'].append(copy.deepcopy(standard_param))
                current_dict['params'][-1]['name'] = par
        if verbose: print(i, content[:2], params, '\n')
        i += 1


    remove = [e for e in data if data[e] == {}]
    print('empty h1`s (removed them):', remove)
    for e in remove:
        del(data[e])

    flattened_data = {}

    print('INFO: saving parsed data into ./jsons/routes_flattened.json')
    cmd_i = 0
    for h1key in data:
        for h2key in data[h1key]:
            for cmd in data[h1key][h2key]:
                flattened_data[cmd_i] = cmd
                cmd_i += 1

    with open('./jsons/routes.json', 'w') as fp:
        json.dump(data, fp)
    with open('./jsons/routes_flattened.json', 'w') as fp:
        json.dump(flattened_data, fp)

    with open('./jsons/routes.json', 'r') as fp:
        d = json.load(fp)

    #printer(data)

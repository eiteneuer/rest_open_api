# REST-Api

The REST-API is Version Controlled via BitBucket and can be cloned at

    git clone git@bitbucket.org:eiteneuer/rest_open_api.git

## Editing the yaml file

The API is generated using a Swagger Macro within Confluence. A good introduction to Swagger and Open API can be found at [this introduction](https://idratherbewriting.com/learnapidoc/pubapis_openapi_tutorial_overview.html). Its input is text formatted as a yaml file. This text is found in `rest-api.yaml`. To edit / generate the `rest-api.yaml`:

 * edit `./jsons/routes_descriptions.json` (explained below)
 * edit `./yamls/parameters.yaml` (explained below)
 * edit `./yamls/tags.yaml` (explained below)

then execute

    python read_in_routes_txt.py
    python create_paths.py

which will create the `rest-api.yaml` in the root folder. This file can then be
used in Confluence, by copy pasting the contents in the Swagger Macro in
Confluence at "Internal Support / How To / AconitySTUDIO API's / REST-API".`It is important to push the changes to the repository, which is done via

    git add jsons yamls rest-api.yaml
    git commit -m 'update rest-api.yaml'
    git push

## routes_descriptions.json

The `./jsons/routes_descriptions.json` file will get read in and used in the
main file. The Information for one route looks like this:

     "dme.rest.api.ApplicationController.login": {
        "url": "post /login",
        "tags": "Authentication",
        "summary": "login",
        "description": "login description",
        "return_type": "json"
     },


 * *url*: What is written here will not be used, but is included for
   Convenience. For instance, you might wish to know that
   `dme.rest.api.ApplicationController.login`, the route which you are editing
   gets called when the html request POST `/login` is called.
 * *tags*: Every single route gets such a tag. All routes with the same tag will
   be grouped together later.
 * *summary*: short description, visible even when route is collapsed
 * *description*: long description, is not visible when route is collapsed
 
## parameters.yaml

This file is found in the yaml folder and includes the standard explanations for all parameters used in the
html requests. Example of parameters are: `job_id`, `project_id`.

One example looks like this:


    project_id:
      name: project_id
      in: paths
      required: true
      type: string
      description: project

This definition will be used in the yaml file whenever `..ref..` is called (see below)

      /projects/:project_id:
        get:
          tags:
          - "Project"
          summary: "get summary"
          description: "get description"
          operationId: "studio.rest.project.ProjectController.get"
          consumes:
            - "application/json"
          produces:
            - "json"
          parameters:
            - $ref: "#/parameters/project_id"
          responses:
            405:
              description: "Invalid input"

## tags.yaml

Tags are used to group different routes together. For example, the routes `/login` and
`/logout` both are tagged with __Authentication__ and will be grouped together.
`tags.yaml` is found in the yaml folder and provides functionality to put a short description for each grouping of routes. For instance, the description for __Authentication__  could be "everything about login and logout".
It looks like this

    tags:
      - name: Authentication
        description: "Everything about login/logout"
      - name: Configuration
        description: "will get displayed right next to Configuration"
      - name: something else
        description: "etc..."
